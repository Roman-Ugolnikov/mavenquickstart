### What is this repository for? ###

this is repo for solving next task: 

## Task ##
<div class="challenge-text hrx-version ck_table-wrap fadeinContent" style="min-height:100px;"><p>Mr. Octopus has recently shut down his factory and wants to sell off his metal bars to a local businessman.&nbsp;<span style="font-family:whitney ssm a,whitney ssm b,verdana,lucida grande,sans-serif; font-size:16px; font-style:normal; font-variant:normal; line-height:24px">&nbsp;Mr. Octopus has many bars whose length are represented by array -&nbsp;<em>lengths = {lengths[0], &nbsp;lengths[1], lengths[2], ...}.</em></span><br>

<br>
The local businessman will only pay for bars that have same length. Let's say Mr. Octopus plans to sell bars of length <em>L</em>&nbsp;only. Then he had to cut each bar 0 or more time, so that he can maximize the profit. The remaining bars whose length is not&nbsp;<em>L</em>&nbsp;will be thrown away. Price of&nbsp;<em>N</em>&nbsp;bars of length&nbsp;<em>L&nbsp;</em>will be&nbsp;<em style="font-family:whitney ssm a,whitney ssm b,verdana,lucida grande,sans-serif; font-size:16px; font-variant:normal; line-height:24px">N&nbsp;×&nbsp;L&nbsp;×&nbsp;unit_price.&nbsp;</em>Also note that for each cut made to a bar, he had to pay&nbsp;<em>cut_cost</em>.</p>

<p><br>
What is the maximum amount of money Mr. Octopus can make? You have to complete the function <em>int maxProfit(int cut_cost, int unit_price, int[] lengths).</em><br>
&nbsp;</p>

<p><strong>Constraints:</strong></p>

<ul>
    <li><em>lengths</em> will contain between 1 to 50 elements, inclusive.</li>
    <li>Each element of <em>lengths</em> will lie in range [1, 10,000].</li>
    <li>1 ≤ <em>unit_price, cut_cost ≤ 1,000.</em></li>
</ul>

<p>&nbsp;</p>

<p><strong>Input Format:</strong></p>

<p>The function "<em style="font-variant: normal; line-height: 24px; font-family: 'Whitney SSm A', 'Whitney SSm B', verdana, 'Lucida Grande', sans-serif; font-size: 16px;">maxProfit"&nbsp;</em>contains three arguments&nbsp;<em style="font-variant: normal; line-height: 24px; font-family: 'Whitney SSm A', 'Whitney SSm B', verdana, 'Lucida Grande', sans-serif; font-size: 16px;">(int cut_cost, int unit_price, int[] lengths),</em>&nbsp;cut_cost, unit_price, length array respectively.</p>

<p>&nbsp;</p>

<p><strong>Output Format:</strong></p>

<p>Return an integer denoting the maximum profit that can be gained by Mr. Octopus.</p>

<p>&nbsp;</p>

<p><strong>Sample Input #00:</strong><br>
cut_cost = 1<br>
unit_price = 10<br>
lengths = [26, 103, 59]</p>

<p>&nbsp;</p>

<p><strong>Sample Output #00:</strong><br>
1770</p>

<p>&nbsp;</p>

<p><strong>Explanation #00:</strong><br>
Since cuts are pretty cheap, we can make large number of cuts to reduce the amount of waste. The optimal length of bars will be 6. We can cut 4 pieces of length 6 from 1st bar, and throw piece of length 2, then cut 17 pieces of length 6 from 2nd bar and throw away a piece of length 1. From the third bar, we cut 9 pieces of length 6 and throw a piece of length 5. So in total we have 30 pieces of length 6 and we have made 30 cuts also. So total profit is 30×6×10 - 30×1 = 1770</p>

<p>&nbsp;</p>

<p><strong>Sample Input #01:</strong><br>
cut_cost = 100<br>
unit_price = 10<br>
lengths = [26, 103, 59]</p>

<p><br>
<strong>Sample Output #01:</strong><br>
1230</p>

<p><br>
<strong>Explanation #01:</strong><br>
Here we will throw smallest bar entirely and cut the pieces of length 51 from both left. So profit is 3*51*10 - 3*100 = 1230.</p>
</div>