package MavenQuickStart;

/**
 * Main class
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        //init data <start>
        int costPerCut = 100;
        int unitPrice = 10;
        int[] lengths = {26, 103, 59};
        //init data <end>
        System.out.println("max profit is " + Engine.maxProfit(costPerCut, unitPrice, lengths));
    }
}

