package MavenQuickStart;

import java.util.*;

/**
 * Created by roman on 18.07.15.
 */
public class Engine {
    /**
     * get Max pr0fit
     * @param costPerCut
     * @param unitPrice
     * @param lengths
     * @return
     */
    static Map.Entry<Integer, Integer> maxProfit(int costPerCut, int unitPrice, int[] lengths) {

        int[] possibleLengths = getPossibleBarLengths(lengths);
        /** key length, value price */
        SortedMap<Integer, Integer> pricesFromLength = getPricesFromLength(possibleLengths, lengths, unitPrice, costPerCut);
        Map.Entry<Integer, Integer> maxEntry = pricesFromLength.entrySet().stream()
                .max((entry1, entry2) -> entry1.getValue().compareTo(entry2.getValue()))
                .get();
        return maxEntry;
    }

    /**
     * calculate price for each length
     *
     * @param possibleLengths list of possible lengths
     * @return list of prices for each length. (key length, value price)
     */
    private static SortedMap<Integer, Integer> getPricesFromLength(int[] possibleLengths, int[] bars,
                                                                   int priceForUnit, int costPerCut) {
        SortedMap<Integer, Integer> result = new TreeMap();
        for (int i = 1; i < possibleLengths.length; i++) {
            int barLength = i;

            int cutTimes = getCutCounts(bars, barLength);
            int barCounts = getBarCounts(bars, barLength);
            int profit = barCounts * priceForUnit * barLength - costPerCut * cutTimes;
            result.put(barLength, profit);
            System.out.println("length: " + barLength + ", profit: " + profit);
        }
        return result;
    }

    private static int getBarCounts(int[] bars, int barLength) {
        int totalNewBarCount = 0;
        for (int bar : bars) {
            int countFromOneBar = bar / barLength;
            totalNewBarCount = totalNewBarCount + countFromOneBar;
        }
        return totalNewBarCount;
    }

    private static int getCutCounts(int[] bars, int barLength) {
        int totalCutCount = 0;
        for (int bar : bars) {
            int countFromOneBar = bar / barLength;
            if (countFromOneBar != barLength) { //there was no cut
                if (countFromOneBar * barLength < bar) {
                    totalCutCount = totalCutCount + countFromOneBar;
                } else {
                    totalCutCount = totalCutCount + countFromOneBar - 1;
                }
            }
        }
        return totalCutCount;
    }

    private static int[] getPossibleBarLengths(int[] lengths) {
        SortedSet<Integer> lengthSet = new TreeSet<>();
        for (int length : lengths) {
            lengthSet.add(length);
        }
        int max = lengthSet.first();
        int min = lengthSet.last();
        int[] possibleLengthes = new int[min - max];
        //get grades with step 1;
        for (int i = min; i <= max; i++) {
            possibleLengthes[i - min] = i;
        }
        return possibleLengthes;
    }
}
