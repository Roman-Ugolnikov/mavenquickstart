package MavenQuickStart;

import java.util.Map;
import junit.framework.TestCase;

/**
 * Unit test for simple Main.
 */
public class MainTest
        extends TestCase {


    /**
     * <p><strong>Sample Input #01:</strong><br>
     cut_cost = 100<br>
     unit_price = 10<br>
     lengths = [26, 103, 59]</p>

     <p><br>
     <strong>Sample Output #01:</strong><br>
     1230</p>

     * Rigourous Test :-)
     */
    public void testMaxProfit1() {
        int costPerCut = 100;
        int unitPrice = 10;
        int[] lengths = {26, 103, 59};
        //init data <end>
        System.out.println("max profit is " + Engine.maxProfit(costPerCut, unitPrice, lengths));
        Map.Entry<Integer,Integer> result = Engine.maxProfit(costPerCut, unitPrice, lengths);
        assertEquals(new Integer(1230), result.getValue());
    }

    /**
     <p>&nbsp;</p>

     <p><strong>Sample Input #00:</strong><br>
     cut_cost = 1<br>
     unit_price = 10<br>
     lengths = [26, 103, 59]</p>

     <p>&nbsp;</p>

     <p><strong>Sample Output #00:</strong><br>
     1770</p>

     <p>&nbsp;</p>
     */
    public void testMaxProfit2() {
        int costPerCut = 1;
        int unitPrice = 10;
        int[] lengths = {26, 103, 59};
        //init data <end>
        System.out.println("max profit is " + Engine.maxProfit(costPerCut, unitPrice, lengths));
        Map.Entry<Integer,Integer> result = Engine.maxProfit(costPerCut, unitPrice, lengths);
        assertEquals(new Integer(1770), result.getValue());
    }
}
